

// console.log("hello from JS file"); 

//form component and place it inside a new variable
let registerForm = document.querySelector("#registerUser")

//inside the first param of the method describe the event that it will listen to, while inside the 2nd parameter lets describe the action/ procedure that will happen upon triggering the said event. 
registerForm.addEventListener("submit", (pangyayari) => {
    pangyayari.preventDefault() //to avoid page refresh/page redirection once that the said event has been triggered. 

    //capture each values inside the input fields first, then repackage them inside a new variable
	let firstName = document.querySelector("#firstName").value
	//lets create a checker to make sure that we are successful in captring the values.
	// console.log(firstName)
	let lastName = document.querySelector("#lastName").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(lastName)
	let email = document.querySelector("#userEmail").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(email)
	let mobileNo = document.querySelector("#mobileNumber").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(mobileNo)
	let password = document.querySelector("#password1").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(password)
	let verifyPassword = document.querySelector("#password2").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(verifyPassword)
    
    //imple alert message.
   	// alert("successfully captured data")
   	//the fire() will allow you to trigger the pop up box
    // Swal.fire({
    // 	icon: "success",
    // 	text: "Registered Successfully"
    // })
    // sweet alert only accepts string datatype.
    // Swal.fire("Kahit ano.")
    /*Swal.fire(
    	// describe the structure of the card inside the object
	    {
	    	icon : "success",
	    	title : "Yaaaay",
	    	text : "Successfully Registered"
	    }
    )*/

 
    

    if((firstName !=="" && lastName !=="" && email !=="" && password !=="" && verifyPassword !=="" ) && (password === verifyPassword) && (mobileNo.length === 11)){

        // to intrigate the email-exist method
        // send out a new request
        // check the email value if available or not.
        // upon sending this request, we're instantiating a romise object, which means we should apply a method to handle the response.
        fetch('http://localhost:3000/api/users/email-exists', {
            method : 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                email : email
            })
        }).then(res => {
            return res.json() // make readable once the response returns to the client side.
        }).then(convertedData => {
            // response look like.
            // control structure to determine the proper procedure depending on the response
            if (convertedData === false) {
                // will allow 
                fetch('http://localhost:3000/api/users/register', {
                    // structure of the request for register.
                    method : 'POST',
                    headers : {
                        'Content-Type' : 'application/json'
                    },
                    // API only accepts request in a string format
                    body : JSON.stringify({
                        firstName : firstName,
                        lastName : lastName,
                        email : email,
                        mobileNo : mobileNo,
                        password : password
                    })
                }).then( res => {
                    console.log(res)
                    // console.log("hello")
                    return res.json()
                }).then(data => {
                    console.log(data)
                    if(data === true){
                        Swal.fire("New Account Registered Successfully.")
                    }
                    else{
                        Swal.fire("Something went wrong during registration.")
                    }
                })
            } 
            else {
                Swal.fire("The Email Already Exist!")
            }
        })

        // let pass = /^[0-9a-zA-Z]+$/;
    	// Swal.fire("Cleared");
    	// 1st param : url -> destination of the request
    	
    }

    else{
    	Swal.fire("Oh no!");
    }
})


