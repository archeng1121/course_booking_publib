
// console.log("hello")

let formSubmit = document.querySelector('#createCourse')

// event that will applied in the form component
// create a subfunction inside the method to describe the procedures/action that will take place upon triggering the event.
formSubmit.addEventListener("submit", (mangyayari) => {
	mangyayari.preventDefault() //this will avooid page direction

	let name = document.querySelector("#courseName").value
	let cost = document.querySelector("#coursePrice").value
	let desc = document.querySelector("#courseDesc").value

	// checker if able to capture the values of each input fields
	/*console.log(name)
	console.log(cost)
	console.log(desc)*/

	if(name !=="" && cost !=="" && desc !=="") {
		fetch('http://localhost:3000/api/courses/course-exists', {
			method : 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                name : name
            })
		}).then(res => {
			return res.json()
		}).then(convertedData => {
			if (convertedData === false) {
				// send request to the backend project to process the data for creating a new entry inside the courses collection
				fetch('http://localhost:3000/api/courses/create', {
					method : 'POST',
					headers : {
						'Content-Type' : 'application/json'
					},
					body : JSON.stringify ({
						// properties of the document that the user needs to fill
						// pass the value of the form component inside the request body
						name : name,
						description : desc,
						price : cost
					})
				}).then(res => {
					// handle the promise object that will returned once the fetch method event has hapened.
					// to ans the question below, observed first the structure of the response
					console.log(res)
					return res.json() //why need to convert the response into json format?
				}).then(info => {
					 console.log(info)
					// create control structure  that will describe the response of the UI to the client
					if (info === true) {
						Swal.fire("Course added successfully!")
					}
					else {
						Swal.fire("Something went wrong during adding course.")
					}
				})// handle the promise object that will be returned onced the fetched method event happen
			}
			else {
				Swal.fire("Course Already Exist.")
			}
		})
	}
	else {
		Swal.fire("Fill up all the fields.")
	}
})
