
// console.log("hello login")

// target the form component inside document
let loginForm = document.querySelector('#loginUser');

// login form will be used by the client to inserts his/her account to be authenticated by the app
loginForm.addEventListener("submit", (pagpasok) => {
	pagpasok.preventDefault()

	let email = document.querySelector('#userEmail').value
	let pass = document.querySelector('#password').value

	// checker to confirm the acquired values
	/*console.log(email)
	console.log(pass)*/

	// to validate the data inside the input fields first
	if (email == "" || pass == "") {
		alert("Please input your email and / or password first.")
	} 
	else {
		// send a request to the desired end point
		fetch('http://localhost:3000/api/users/login', {
			method :'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify ({
				email : email,
				password : pass
			})
		}).then(res => {
				return res.json()
			}).then(dataConverted => {
				// check if conversion of the response  has been successful.
				console.log(dataConverted.accessToken)
				// save the generated access token inside the local storage property of the browser.

				if (dataConverted.accessToken) {
					localStorage.setItem('token', dataConverted.accessToken)
					// alert("Success!");

					// before redirecting the user to the next location, we must identify the access rights that we will grant for that user

					// how can we know if the user is an admin or not
					fetch('http://localhost:3000/api/users/details', {
						headers : {
							// pass the value of access token.
							'Authorization' : `Bearer ${dataConverted.accessToken}`
						}// upon sending this request, we are instatiating a promise that can lead to 2 possible outcomes, what do we need to do to handle the possible outcome states of this promise.
					}).then(res => {
						return res.json()
					}).then(data => {
						// check if we re able to get the user's data
						console.log(data)
						// fetching the user details/info is a success
						// save the "id", "isAdmin"
						localStorage.setItem("id", data._id)
						localStorage.setItem("isAdmin", data.isAdmin)
						// as developers, it's up to you if you want to create a checker to make sure that all values are saved properly inside the web storage
						// console.log('items are set inside the local storage')
					})
					// redirecting to next location
					window.location.replace('./profile.html')
				} 
				else {
					// this block of code will run if no access token was generated.
					alert('No access token generated.');
				}
				
				
				
			})
	}
})

